const SEND_MESSAGE = 'SEND-MESSAGE';
const UPDATE_NEW_MESSAGE_BODY = 'UPDATE-NEW-MESSAGE-BODY';

let initialState = {
    messages:[
        {id: 1, message: 'Hey'},
        {id: 2, message: "What's up?"},
        {id: 3, message: 'How are you doing?'},
        {id: 4, message: 'Yes, sure'},
        {id: 5, message: 'Yo, bro'}
      ],
      newMessageBody: "",
      dialogs:[
        {id: 1, name: 'Richard', photo: "https://square-vn.com/app/dscms/assets/images/person-3.jpg?v=1653932875"},
        {id: 2, name: 'Tanya', photo: "https://images.ctfassets.net/1wryd5vd9xez/6imn4PsoUBr6I9Hs8jWxk4/b28965e1afec63588266cf42ba5178ae/https___cdn-images-1.medium.com_max_2000_1_7hkI-ZKsglnbjxCRV1bMZA.png"},
        {id: 3, name: 'Christofer', photo: "https://images.fastcompany.net/image/upload/w_596,c_limit,q_auto:best,f_auto/fc/3021752-inline-i-1-why-square-designed-its-new-offices-to-work-like-a-city.jpg"},
        {id: 4, name: 'Lilly', photo: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVm9O_h7BwrXqWA2C5uj0QmTap-3axjAnpV1VPymvjgkfRnuJVvSR_tI00WLn_9bg4gCM&usqp=CAU"},
        {id: 5, name: 'Anna', photo: "https://images.squarespace-cdn.com/content/v1/5a15a083fe54efb29618b6d7/1514910364548-H0SJN436IXDQOKIV6QFX/Rebecca-Rosen-portrait-sm.jpg"},
        {id: 6, name: 'Susan', photo: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTElj1CuBjJGNsFD-xslkdEaiwT9bX-sjO5Lg&usqp=CAU"}
      ]
};

const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_NEW_MESSAGE_BODY:
            return {
                ...state,
                newMessageBody: action.body
            }
        case SEND_MESSAGE:
            let body = state.newMessageBody;
            return {
                ...state,
                newMessageBody:'',
                messages: [...state.messages, {id: 6, message: body}]
            };
        default:
            return state;
    }
}

export const sendMessageCreator = () => ({type: SEND_MESSAGE});

export const updateNewMessageBodyCreator = (body) => 
({type: UPDATE_NEW_MESSAGE_BODY, body: body })

export default dialogsReducer;