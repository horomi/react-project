import profileReducer  from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import sideBarReducer from "./sidebar-reducer";


let store = {
  _state: {
    profilePage: {
      posts:[
        {id: 1, message: 'Hey, how are you doing?', likesCount: 0},
        {id: 2, message: "It is my first post.", likesCount: 23},
        {id: 3, message: "Bla-bla", likesCount: 3},
        {id: 4, message: "My day...", likesCount: 19}
      ],
      newPostText: "chaging test"
    },
    dialogsPage: {
      messages:[
        {id: 1, message: 'Hey'},
        {id: 2, message: "What's up?"},
        {id: 3, message: 'How are you doing?'},
        {id: 4, message: 'Yes, sure'},
        {id: 5, message: 'Yo, bro'}
      ],
      newMessageBody: "",
      dialogs:[
        {id: 1, name: 'Richard', photo: "https://square-vn.com/app/dscms/assets/images/person-3.jpg?v=1653932875"},
        {id: 2, name: 'Tanya', photo: "https://images.ctfassets.net/1wryd5vd9xez/6imn4PsoUBr6I9Hs8jWxk4/b28965e1afec63588266cf42ba5178ae/https___cdn-images-1.medium.com_max_2000_1_7hkI-ZKsglnbjxCRV1bMZA.png"},
        {id: 3, name: 'Christofer', photo: "https://images.fastcompany.net/image/upload/w_596,c_limit,q_auto:best,f_auto/fc/3021752-inline-i-1-why-square-designed-its-new-offices-to-work-like-a-city.jpg"},
        {id: 4, name: 'Lilly', photo: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVm9O_h7BwrXqWA2C5uj0QmTap-3axjAnpV1VPymvjgkfRnuJVvSR_tI00WLn_9bg4gCM&usqp=CAU"},
        {id: 5, name: 'Anna', photo: "https://images.squarespace-cdn.com/content/v1/5a15a083fe54efb29618b6d7/1514910364548-H0SJN436IXDQOKIV6QFX/Rebecca-Rosen-portrait-sm.jpg"},
        {id: 6, name: 'Susan', photo: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTElj1CuBjJGNsFD-xslkdEaiwT9bX-sjO5Lg&usqp=CAU"}
      ]
    },  
    sideBar: {
      friends: [
        {id: 1, name: 'Hanna', photo: "https://content.latest-hairstyles.com/wp-content/uploads/medium-haircuts-for-women-with-square-face.jpg"},
        {id: 2, name: 'Merry', photo: "https://i0.wp.com/www.hadviser.com/wp-content/uploads/2019/04/28-natural-waves-for-square-face-BaFK4-OgHH8.jpg?resize=987%2C1038&ssl=1"},
        {id: 3, name : 'Harry', photo: "https://stylesatlife.com/wp-content/uploads/2019/09/Edgy-Side-Look-for-Men.jpg.webp"}
      ]
    }
  },
  _callSubscriber(){
    console.log("no subscribers")
  },
  getState(){
    return this._state
  },
  subscribe(observer) {
    this._callSubscriber = observer;
  },

  dispatch(action) {
    this._state.profilePage = profileReducer(this._state.profilePage, action);
    this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
    this._state.sideBar = sideBarReducer(this._state.sideBar, action);

    this._callSubscriber(this._state);
    
  }
}


export default store;