const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';
const SET_USER_PROFILE = 'SET_USER_PROFILE';

let initialState = {
    posts:[
        {id: 1, message: 'Hey, how are you doing?', likesCount: 0},
        {id: 2, message: "It is my first post.", likesCount: 23},
        {id: 3, message: "Bla-bla", likesCount: 3},
        {id: 4, message: "My day...", likesCount: 19}
      ],
      newPostText: "it-kama",
      profile: null,
};

const profileReducer = (state = initialState, action) => {
    switch (action.type){
        case ADD_POST:
            let newPost = {
                id: 5,
                message: state.newPostText,
                likesCount: 0
            };
            return {
                ...state,
                posts: [...state.posts, newPost],
                newPostText: ''
            };
        case UPDATE_NEW_POST_TEXT:
            return {
                ...state,
                newPostText: action.newText,
            };
        case SET_USER_PROFILE:
            debugger
            return {...state, profile: action.profile};

        default:
            return state;
    }
}

export const addPostActionCreator = () => ({ type: ADD_POST});
export const setUserProfile = (profile) => ({ type: SET_USER_PROFILE, profile});
export const updateNewPostTextActonCreator = (text) => 
({ type: UPDATE_NEW_POST_TEXT, newText: text })

export default profileReducer;