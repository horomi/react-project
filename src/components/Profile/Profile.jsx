import MyPostsContainer from './MyPosts/myPostsContainer';
import profclass from './Profile.module.css'
import ProfileInfo from './ProfileInfo/ProfileInfo';
import React from 'react';

const Profile = (props) => {

    return (
        <div className={profclass.content}>
          <ProfileInfo profile={props.profile} />
          < MyPostsContainer />
        </div>
      );
    }


export default Profile;