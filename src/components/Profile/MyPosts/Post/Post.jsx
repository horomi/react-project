import postclass from './Post.module.css'
import React from 'react';

const Post = (props) => {
    return(
        <div className={postclass.post}>
            <div className={postclass.item}>
                <img src='https://pyxis.nymag.com/v1/imgs/a3b/f23/e3624f93486cc9f40a65f1ca7fd6887581-27-angelina-jolie.rsquare.w700.jpg' alt='avatar'></img>
                {props.message}
                <div>
                    Likes {props.likesCount}
                </div>
                
            </div>
        </div>
    )
}

export default Post;