import profclass from './ProfileInfo.module.css'
import React from 'react';
import Preloader from '../../common/Preloader/Preloader';


const ProfileInfo = (props) => {
    if (!props.profile) {
        return <Preloader />
    }
    return(
        <div>
            <img className={profclass.mainPicture} src='https://kartinkin.net/uploads/posts/2021-07/1626730815_2-kartinkin-com-p-fon-sots-seti-krasivo-2.jpg' alt='oops'/>
            <div className={profclass.descriptionBlock}>
                <img src={props.profile.photos.large} alt="pho"></img>
                ava+description
            </div>
        </div>
    );
}

export default ProfileInfo