import classes from './DialogItem.module.css';
import { NavLink } from 'react-router-dom';
import React from 'react';

const DialogItem = (props) => {
    let path = "/dialogs/" + props.id
    return (
        <div className={classes.dialog}>
            <div><img src={props.photo} alt='avatar'></img>
            <NavLink to={path}>{props.name}</NavLink></div>
        </div>
    );
}

export default DialogItem;
