import React from "react";
import { NavLink } from 'react-router-dom';
import classes from './Users.module.css'
import userPhoto from '../../assets/images/user-photo.png';


let Users = (props) => {
        let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);

        let pages = [];
        for (let i = 1; i <= pagesCount; i++) {
            pages.push(i);
        }
        let slicedPages;
        let curPage = props.currentPage;
        if (curPage - 3 < 0) {
            slicedPages = pages.slice(0, 5);
        } else {
            slicedPages = pages.slice(curPage - 3, curPage + 2);
        }

        return <div className={classes.users}>
            <div >
                {slicedPages.map((p) => {
                    return <span key={p} className={props.currentPage === p ? classes.selectedPage : classes.pages} 
                    onClick={(e) => {props.onPageChanged(p)}}>{p}</span>
                })}
            </div>
            {
            props.users.map(u => <div key={u.id}>
            
            <span className={classes.userItem}>
                <div>
                    <NavLink to={"/profile/" + u.id}>
                        <img src={u.photos.small != null ? u.photos.small: userPhoto} className={classes.userPhoto} alt='avatar'/>
                    </NavLink>
                </div>
                <div>
                    { u.followed 
                    ? <button className={classes.btnFollow} onClick={() => { props.unfollow(u.id) }} >Unfollow</button> 
                    : <button className={classes.btnUnfollow} onClick={() => { props.follow(u.id) }} >Follow</button> }
                </div>
            </span>
            <span className={classes.userData}>
                <span>
                    <div>{u.name}</div>
                    <div>{u.status}</div>
                </span>
                <span>
                    <div>{"u.location.country"}</div>
                    <div>{"u.location.city"}</div>
                </span>
            </span>

        </div>)
        }
        </div>;
    }


export default Users;