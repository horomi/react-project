import classes from './Navbar.module.css'
import { NavLink } from 'react-router-dom';
import FriendsContainer from './Friends/FriendsContainer';
import React from 'react';

const setActive = ({isActive}) => isActive ? classes.activeLink: classes.item;

const Navbar = (props) => {
    debugger
    return (
        <nav className={classes.nav}>
            <div className={classes.item}>
                <NavLink to="/profile" className={setActive}>Profile</NavLink>
            </div>
            <div className={classes.item}>
                <NavLink to="/dialogs" className={setActive}>Messages</NavLink>
            </div>
            <div className={classes.item}>
                <NavLink to="/users" className={setActive}>Users</NavLink>
            </div>
            <div className={classes.item}>
                <a href='/news'>News</a>
            </div>
            <div className={classes.item}>
                <a href='/music'>Music</a>
            </div>
            
            <div className={classes.FriendsBlock}>
                My friends: 
                <FriendsContainer />
            </div>
            
      </nav>
    );
}

export default Navbar