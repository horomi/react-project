import classes from './Friends.module.css';
import React from 'react';


const Friends = (props) => {
    
    let friendElement = props.friends.friends.map(f => (
        <div key={f.id} className={classes.friendItem}>
            <img src={f.photo} alt="avatar-friend" />
            {f.name}
        </div>)
        );

    return (
        <div className={classes.myFriends}>
            {friendElement}
        </div>
    )
}

export default Friends;