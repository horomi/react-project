import Friends from './Friends';
import { connect } from 'react-redux';


const mapStateToProps = (state) => {
    return {
        friends: state.sideBar
    }
}

const FriendsContainer = connect(mapStateToProps)(Friends);

export default FriendsContainer;