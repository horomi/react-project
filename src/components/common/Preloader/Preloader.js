import React from "react";
import preloader from '../../../assets/images/preloader.svg';

let Preloader = (props) => {
    return <div>
        <img src={preloader} alt='loader' height='100px'/>
    </div>
}

export default Preloader;